#pragma once

#include <type_traits>

template< typename T >
requires std::is_same_v< T, void >
struct Show {};
