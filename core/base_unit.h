#pragma once

#include "compile_time_str.h"
#include "is_instantiation_of.h"
#include "rational.h"


/**
 * *Base units* are those that cannot be derived from other units: e.g., kilogram, meter and second
 * are base units in the SI system, whereas joule = kg * m^2 / s^2 is not.
 *
 * All base unit types must be instantiations of this template.
 */
template< CompileTimeStr symbol >
struct DefineBaseUnit {

    static_assert( hasLetter( symbol ) );

    static consteval auto str() { return symbol; }
};

template< typename T >
concept BaseUnit = IsInstantationOfNTT<T, DefineBaseUnit>;


using boost::mp11::mp_bool;

template< BaseUnit U1, BaseUnit U2 >
using LessBySymbol = mp_bool< U1::str() < U2::str() >;


/**
 * E.g., km = 1000 * m; hour = 3600 * s; g = 0.001 * kg; etc.
 */
template< CompileTimeStr symbol, Rational Mult, BaseUnit U >
struct MultiplyBaseUnit {

    static_assert( hasLetter( symbol ) );

    using Multiplier = Mult;

    using Base = U;

    static consteval auto str() { return symbol; }
};

template< typename T >
concept MultipleOfBaseUnit = IsInstantationOfNTT<T, MultiplyBaseUnit>;

template< MultipleOfBaseUnit M >
using BaseOf = typename M::Base;

template< MultipleOfBaseUnit M >
using MultiplierOf = typename M::Multiplier;


template< typename T >
concept BaseOrMultiple = BaseUnit< T > || MultipleOfBaseUnit< T >;


using boost::mp11::mp_eval_if_c;

template< BaseOrMultiple U >
using Base = mp_eval_if_c< BaseUnit< U >, U, BaseOf, U >;

template< BaseOrMultiple U >
using Multiplier = mp_eval_if_c< BaseUnit< U >, One, MultiplierOf, U >;
