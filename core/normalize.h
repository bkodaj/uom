#pragma once

#include "product_of_unit_powers.h"
#include "mp_utils.h"

#include <boost/mp11.hpp>

#include <utility>

using boost::mp11::mp_same,
      boost::mp11::mp_copy_if_q,
      boost::mp11::mp_front,
      boost::mp11::mp_if_c;

template< BaseUnit B >
struct HasBaseUnit {

    template< UnitPower UP > using fn = mp_same< Base< Unit< UP > >, B >;
};

/**
 * - Assume that
 *
 *     P = (m1 * B) ^ Exp1 * ... * (m_k * B) ^ Exp_k * Unit_{ k+1 } ^ Exp_{ k+1 } * ... * Unit_n ^ Exp_n
 *
 *   where:
 *     - B is a base unit.
 *     - m_1 * B, ..., m_k * B are either B itself (if m_i = 1) or multiples of B (if m_i != 1).
 *     - The base units underlying Unit_{ k+1 }, ..., Unit_n are distinct from B.
 *
 * - Our goal is to replace the product of unit powers containing B, i.e.,
 *
 *      Q = (m1 * B) ^ Exp1 * ... * (m_k * B) ^ Exp_k
 *
 *   with a multiplier times __one__ unit power.
 *
 * - If m1 = m2 = ... = m_k, i.e., all units in Q are identical, then we're done: Q = 1 * (m1 * B) ^ sumExp,
 *   where sumExp = Exp1 + Exp2 + ... + Exp_k.
 *
 *   E.g.,
 *     - m^1 * m^2 =  m^3
 *     - s^1 * s^-1 = s^0
 *     - km^1 * km^2 = km^3
 *
 * - Otherwise, there are two or more distinct units in Q; in this case there's no way for us to know which unit
 *   the user would prefer to keep, so we __revert__ all units to B: Q = m * B ^ sumExp, where
 *
 *      m = m1^Exp1 * m2^Exp2 * ... * m_k^Exp_k
 *
 *   E.g.,
 *     - km^2 * m^-2 = (1000 * m) ^ 2 * m^-2 = 1000^2 * m^0
 *     - km^1 * cm^2 = (1000 * m) ^ 1 * (0.01 * m) ^ 2 = 1000 * 0.01^2 * m^3
 */
template< BaseUnit B, ProductOfUnitPowers P >
requires notEmpty< P > && contains< BaseUnits< P >, B >
struct MultiplierAndUnitPower_T {

    using Q = mp_copy_if_q< P, HasBaseUnit< B > >;

    using FirstUnit = mp_front< Units< Q > >;

    using SumExp = Sum< Exponents< Q > >;

    using Type = mp_if_c< allSame< Units< Q > >,

        std::pair< One, RaiseToPower< FirstUnit, SumExp > >,

        std::pair< ProductOfMultiplierPowers< Q >, RaiseToPower< Base< FirstUnit >, SumExp > >
    >;
};

template< BaseUnit B, ProductOfUnitPowers P >
using MultiplierAndUnitPower = typename MultiplierAndUnitPower_T< B, P >::Type;


using boost::mp11::mp_unique,
      boost::mp11::mp_bind_back,
      boost::mp11::mp_transform_q,
      boost::mp11::mp_apply,
      boost::mp11::mp_sort;

/**
 * @tparam P  Assume that this is
 *
 *               (Unit1 ^ Exp1) * ... * (Unit_n ^ Exp_n)
 *
 *            where the units are not necessarily distinct.
 */
template< ProductOfUnitPowers P >
struct Normalize_T {

    /**
     * Assume that:
     *  - B1, ..., B_k are the distinct base units underlying Unit1, ..., Unit_n.
     *
     *  - For a specific B_i:
     *    - Let Unit_i1, Unit_i2, ... be those units among Unit1, ..., Unit_n whose underlying base unit is B_i.
     *    - Let m_i1, m_i2, ... be the corresponding multipliers (i.e., Unit_ij = m_ij * B_i).
     *    - Let Q_i = (Unit_i1 ^ Exp_i1) * (Unit_i2 ^ Exp_i2) * ...
     *    - Let m_i = (m_i1 ^ Exp_i1) * (m_i2 ^ Exp_i2) * ...
     *    - Let sumExp_i = Exp_i1 + Exp_i2 + ...
     */
    using DistinctBaseUnits = mp_unique< BaseUnits< P > >;

    /**
     * This is
     *
     *     [ ( c1, V1 ^ sumExp1 ), ... ( c_k, V_k ^ sumExp_k ) ]
     *
     * where:
     *   - If Q_i is not reverted to B_i, then c_i = 1 and V_i = Unit_i1 (NB: all Unit_ij's are equal in this case).
     *
     *   - If Q_i is reverted, then c_i = m_i and V_i = B_i.
     *
     * E.g.,
     *  - kg * m * s^-1 * s^-1 --> [ ( 1, kg^1 ), ( 1, m^1 ), ( 1, s^-2 ) ]
     *  - kg * m * s^-1 * hour^-1 --> [ ( 1, kg^1 ), ( 1, m^1 ), ( 3600^-1, s^-2 ) ]
     *  - kg * m * s^-1 * hour --> [ ( 1, kg^1 ), ( 1, m^1 ), ( 3600, s^0 ) ]
     *  - km * hour^-1 --> [ ( 1, km^1 ), ( 1, hour^-1 ) ]
     *  - km * m * hour^-1 --> [ ( 1000, m^2 ), ( 1, hour^-1 ) ]
     */
    using MultipliersAndUnitPowers = mp_transform_q <

        mp_bind_back< MultiplierAndUnitPower, P >,

        DistinctBaseUnits
    >;

    using FinalMultiplier = ApplyToFirst< MultiplyRationals, MultipliersAndUnitPowers >;

    using NonZeroUnitPowers = FilterSecond< ExponentCondition< IsNonZero >, MultipliersAndUnitPowers >;

    using Type = mp_apply< MultiplyUnitPowers, mp_sort< NonZeroUnitPowers, LessByBaseUnitSymbol > >;

    static_assert( NormalizedProductOfUnitPowers< Type > );
};


/**
 * *Normalization* means that we take a product of unit powers and turn it into the following form:
 *
 *    m * (U1 ^ e1) * (U2 ^ e2) * ...
 *
 * where:
 *  - m is a constant multiplier.
 *  - U1, U2, ... are base units or multiples of base units, and:
 *    - Their underlying base units are distinct.
 *    - They are in a specific order (determined via comparing their base units).
 *  - e1, e2, ... are nonzero.
 *
 *  Note that the (U1 ^ e1) * ... product is allowed to be empty, in which case the result of normalization
 *  is just `m` as a dimensionless constant.
 *
 *  E.g.,
 *  - m * kg * s * m --> 1 * kg * m^2 * s
 *  - km * km --> 1 * km^2
 *  - m * km * cm --> 10 * m^3
 *  - s * s^-1 --> 1
 *  - h * s^-1 --> 3600
 */
template< ProductOfUnitPowers P >
using Normalize = typename Normalize_T< P >::Type;

template< ProductOfUnitPowers P >
inline constexpr auto multiplier = toDouble< typename Normalize_T< P >::FinalMultiplier >;
