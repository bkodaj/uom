#pragma once

#include "is_instantiation_of.h"
#include "negatives_in_denom.h"
#include "rational.h"
#include "unit_power.h"

#include <boost/mp11.hpp>


using boost::mp11::mp_list;

/**
 * A product whose factors are powers of base units (or of multiples of base units).
 *
 * Purpose:
 * - This is the basis for representing *derived* units like m * s^-1, kg * m^-3, km * h^-1, etc.
 *
 * - It also serves as an intermediate structure when computing products of units.
 *   E.g. ( kg * m * s^-2 ) * m --> kg * m * s^-2 * m --> kg * m^2 * s^-2.
 *
 * Notes:
 * - The product is allowed to be empty: in that case, it represents the dimensionless constant 1.
 *
 * - The product is allowed to contain distinct multiples of the same base unit. E.g., km * cm * dm * m^-1 is
 *   legal here (but it's illegal as a derived unit).
 */
template< UnitPower... UPs >
struct MultiplyUnitPowers {

    using Factors = mp_list< UPs... >;

    using Units = mp_list< Unit< UPs >... >;

    using BaseUnits = mp_list< Base< Unit< UPs > > ... >;

    using Exponents = mp_list< Exponent< UPs >... >;

    static consteval auto str() {

        return NegativesInDenom< UPs... >::str();
    }
};

template< typename T >
concept ProductOfUnitPowers = IsInstantationOf<T, MultiplyUnitPowers>;

template< ProductOfUnitPowers P >
using Factors = typename P::Factors;

template< ProductOfUnitPowers P >
using Units = typename P::Units;

template< ProductOfUnitPowers P >
using BaseUnits = typename P::BaseUnits;

template< ProductOfUnitPowers P >
using Exponents = typename P::Exponents;


using boost::mp11::mp_apply,
      boost::mp11::mp_append;

template< ProductOfUnitPowers... Ps >
using Multiply = mp_apply< MultiplyUnitPowers, mp_append< Factors< Ps > ... > >;


using boost::mp11::mp_transform;

template< ProductOfUnitPowers P >
using Invert = mp_transform< InversePower, P >;


/**
 * E.g., km * h^-1 --> m * s^-1
 */
template< ProductOfUnitPowers P >
using ProductOfBaseUnitPowers = mp_transform< BaseUnitPower, P >;

/**
 * E.g., km * h^-1 --> 1000 * 3600^-1
 */
template< ProductOfUnitPowers P >
using ProductOfMultiplierPowers = mp_apply< MultiplyRationals, mp_transform< MultiplierPower, Factors< P > > >;


template< BaseOrMultiple B >
using AsUnitPower = MultiplyUnitPowers< RaiseToPower< B, mp_int< 1 > > >;


template< typename T >
concept NormalizedProductOfUnitPowers = ProductOfUnitPowers< T > &&
                                        distinct< BaseUnits< T > > &&
                                        allNonZero< Exponents< T > > &&
                                        sorted< Factors< T >, LessByBaseUnitSymbol >;

