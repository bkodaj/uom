#pragma once

#include <type_traits>

#include <boost/mp11.hpp>


using boost::mp11::mp_int,
      boost::mp11::mp_bool,
      boost::mp11::mp_if,
      boost::mp11::mp_all_of;

/**
 * Expresses the fact that `I` is the type-level representation of an integer constant (think mp_int).
 */
template< typename I >
concept Int = requires {

    typename mp_int< I::value >; // Verifies that I::value is a constant integral expression.
};

template< typename I >
concept NonNegativeInt = Int< I > && I::value >= 0;

template< Int I >
using IsNonZero = mp_bool< I::value != 0 >;

template< Int I >
using IsPositive = mp_bool< ( I::value > 0 ) >;

template< Int I >
using IsNonNegative = mp_bool< ( I::value >= 0 ) >;

template< Int I >
using IsNegative = mp_bool< ( I::value < 0 ) >;

template< Int I >
using Minus = mp_int< -I::value >;

template< Int I >
using Abs = mp_if< IsNonNegative< I >, I, Minus< I > >;

template< typename ListofInts >
inline constexpr auto allNonZero = mp_all_of< ListofInts, IsNonZero >::value;