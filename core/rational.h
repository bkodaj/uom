#pragma once

#include "int.h"
#include "mp_utils.h"

#include <ratio>
#include <type_traits>

#include <boost/mp11.hpp>

template< typename >
struct IsRational : std::false_type{};

template< std::intmax_t Num, std::intmax_t Denom >
struct IsRational< std::ratio< Num, Denom > > : std::true_type{};

template< typename T >
concept Rational = IsRational< T >::value;

template< Rational R >
using Reciprocal = std::ratio< R::den, R::num >;

template< Rational R >
inline constexpr auto toDouble = double( R::num ) / R::den;


using One = std::ratio< 1, 1 >;


using boost::mp11::mp_list,
      boost::mp11::mp_fold;

template< Rational... Numbers >
using MultiplyRationals = mp_fold< mp_list< Numbers... >, One, std::ratio_multiply >;


template< Rational Num, Rational Denom >
using DivideRationals = MultiplyRationals< Num, Reciprocal< Denom > >;


using boost::mp11::mp_apply,
      boost::mp11::mp_if_c;

template< Rational Number, NonNegativeInt Exponent >
using NonNegativePower = mp_apply< MultiplyRationals, RepeatN< Number, Exponent > >;

template< Rational Number, Int Exponent >
using Power = NonNegativePower<

    mp_if_c< NonNegativeInt< Exponent >, Number, Reciprocal< Number > >,

    Abs< Exponent >
>;