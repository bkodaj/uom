#pragma once

#include "compile_time_str.h"
#include "int.h"
#include "is_instantiation_of.h"
#include "mp_utils.h"
#include "product_of_unit_powers.h"

#include <boost/mp11.hpp>


/**
 * A *derived* unit is the product of non-zero integer powers of distinct base units (or of multiples of base units).
 *
 * E.g.,
 *  - kg * m^-3 (the SI unit of density)
 *  - m * s^-2 (the SI unit of acceleration)
 *  - km * h^-1 (assuminmg that km and h are defined as the appropriate multiples of the base units m and s)
 *
 * Notes:
 * - The product is allowed to be empty, in which case it represents the dimensionless number 1, which you can think of
 *   as the unit of measure of dimensionless quantities.
 *
 * - Distinct factors in the product must have distinct base units. E.g., km * m * cm is illegal, but km^2 is OK.
 *
 * - The factors of the product must be in a specific order so that we can ensure that m * s^-1 is considered the same
 *   as s^-1 * m.
 */
template< typename T >
concept DerivedUnit = NormalizedProductOfUnitPowers< T >;

/**
 * E.g.,
 *  - J = kg * m^2 / s^2
 *  - Hz = 1 / s
 */
template< CompileTimeStr symbol, DerivedUnit D >
struct NameDerivedUnit {

    static_assert( hasLetter( symbol ) );

    using Underlier = D;

    static consteval auto str() { return symbol; }
};

template< typename T >
concept NamedDerivedUnit = IsInstantationOfNTT<T, NameDerivedUnit>;

template< NamedDerivedUnit N >
using Underlier = typename N::Underlier;


template< typename T >
concept DerivedOrNamed = DerivedUnit< T > || NamedDerivedUnit< T >;


using boost::mp11::mp_eval_if_c;

template< DerivedOrNamed D >
using UnderlyingUnitPowers = mp_eval_if_c< DerivedUnit< D >, D, Underlier, D >;
