#pragma once

#include "base_unit.h"
#include "derived_unit.h"
#include "product_of_unit_powers.h"
#include "mp_utils.h"
#include "normalize.h"

#include <boost/mp11.hpp>


template< typename T >
concept UnitOfMeasure = BaseOrMultiple< T > || DerivedOrNamed< T >;


/**
 * E.g.,
 *  - m --> m^1  (as a one-factor product)
 *  - km --> km^1 (as a one-factor product)
 *  - J --> kg^1 * m^2 * s^-2 (assuming that J is defined appropriately as a named derived unit)
 */
template< UnitOfMeasure U >
using AsProductOfUnitPowers = EvalEitherOr< BaseOrMultiple< U >, U, AsUnitPower, UnderlyingUnitPowers >;


using boost::mp11::mp_front,
      boost::mp11::mp_bool,
      boost::mp11::mp_size,
      boost::mp11::mp_if_c,
      boost::mp11::mp_eval_if_not;

template< ProductOfUnitPowers P >
using HasExactlyOneUnit = mp_bool< mp_size< Units< P > >::value == 1 >;

template< ProductOfUnitPowers P >
using FirstUnitIfFirstPower = mp_if_c< mp_front< Exponents< P > >::value == 1, mp_front< Units< P > >, P >;

/**
 * The inverse of `AsProductOfUnitPowers`: if a product of unit powers consists of a single base unit
 * (or a multiple thereof) raised to the first power, then formally convert the product into that unit.
 * E.g., s ^ 1 --> s, km ^ 1 --> km, etc.
 */
template< ProductOfUnitPowers P >
using AsUnitIfPossible = mp_eval_if_not< HasExactlyOneUnit< P >, P, FirstUnitIfFirstPower, P >;


template< UnitOfMeasure... Us >
using Product = AsUnitIfPossible< Normalize< Multiply< AsProductOfUnitPowers< Us > ...  > > >;


template< UnitOfMeasure... Us >
inline constexpr auto multiplierInProduct = multiplier< Multiply< AsProductOfUnitPowers< Us > ... > >;


template< UnitOfMeasure U1, UnitOfMeasure U2 >
using Ratio = Product< AsProductOfUnitPowers< U1 >, Invert< AsProductOfUnitPowers< U2 > > >;


template< UnitOfMeasure U1, UnitOfMeasure U2 >
inline constexpr auto multiplierInRatio = multiplierInProduct<

    AsProductOfUnitPowers< U1 >, Invert< AsProductOfUnitPowers< U2 > >
>;


template< UnitOfMeasure U >
using Inverse = AsUnitIfPossible< Invert< AsProductOfUnitPowers< U > > >;


template< UnitOfMeasure U1, UnitOfMeasure U2 >
inline constexpr auto sameBaseUnitPowers = std::same_as<

    ProductOfBaseUnitPowers< AsProductOfUnitPowers< U1 > >,

    ProductOfBaseUnitPowers< AsProductOfUnitPowers< U2 > >
>;


/**
 * If U = ( m1 * B1 ) ^ e1 * ... * ( m_n * B_n ) ^ e_n, where B1, ..., B_n are distinct base units,
 * then the multiplier of U versus the product of the appropriate powers of base units is
 * ( m1 ^ e1 ) * ... * ( m_n ^ e_n ).
 *
 * E.g.,
 *  - km^2  --> 1000^2
 *  - km * h^-1 --> 1000 * 3600^-1
 */
template< UnitOfMeasure U >
using MultiplierVsBasePowers = ProductOfMultiplierPowers< AsProductOfUnitPowers< U > >;
