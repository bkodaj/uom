#pragma once

#include <algorithm>
#include <array>
#include <ostream>


/**
 * Represents a string that is known at compile time.
 *
 * Note: data members are public because objects of this type are used as arguments for non-type template
 * parameters. (See https://en.cppreference.com/w/cpp/language/template_parameters#Non-type_template_parameter .)
 */
template< std::size_t N >
struct CompileTimeStr
{
    static_assert( N > 0 );

    static inline constexpr auto length{ N - 1 };

    constexpr CompileTimeStr( const char ( & strLiteral )[ N ] )
    : _str{ std::to_array( strLiteral ) }
    {}

    constexpr CompileTimeStr( const std::array< char, N > & str )
    : _str{ str }
    {}

    constexpr auto begin() const { return _str.begin(); }

    constexpr auto end() const { return _str.end(); }

    constexpr const char * c_str() const { return _str.data(); }


    const std::array< char, N > _str; /** Zero-terminated C-style string */
};


template< CompileTimeStr s >
inline constexpr auto operator ""_c() { return s; }


template< std::size_t N >
inline std::ostream & operator << ( std::ostream & strm, const CompileTimeStr< N > & s ) {

    return strm << s.c_str();
}


template< std::size_t N >
inline constexpr bool hasLetter( const CompileTimeStr< N > & s ) {

    return std::any_of(
        s.begin(), s.end(),
        []( char c ) { return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'); }
    );
}


template< std::size_t L, std::size_t R >
inline constexpr bool operator == ( const CompileTimeStr< L > & left, const CompileTimeStr< R > & right ) {

    if constexpr( L == R ) {
        return left._str == right._str;
    }
    else {
        return false;
    }
}


template< std::size_t L, std::size_t R >
inline constexpr bool operator < ( const CompileTimeStr< L > & left, const CompileTimeStr< R > & right ) {

    return std::lexicographical_compare( left.begin(), left.end(), right.begin(), right.end() );
}


template< std::size_t N1, std::size_t N2 >
inline constexpr auto operator + ( const CompileTimeStr< N1 > & s1, const CompileTimeStr< N2 > & s2 ) {

    std::array< char, N1 + N2 - 1 > s;

    const auto after1 = std::copy( s1.begin(), s1.end() - 1, s.begin() );

    std::copy( s2.begin(), s2.end(), after1 );

    return CompileTimeStr{ s };
}


template< std::size_t ... Ns >
inline constexpr auto concat( const CompileTimeStr< Ns > & ... strs ) {

    return ( ""_c + ... + strs );
}


template< std::size_t N, std::size_t ... Ns >
inline constexpr auto intersperse( const CompileTimeStr< N > & separator, const CompileTimeStr< Ns > & ... strs ) {

    /**
     * `I` is the index of the given string in `strs`.
     */
    auto sepAndString = [&]< std::size_t I, std::size_t S >( const CompileTimeStr< S > & str ) {

        if constexpr (I > 0) {
            return separator + str;
        } else {
            return str;
        }
    };

    auto helper = [&]< std::size_t... Is >( std::index_sequence< Is... > ) {

        return concat( sepAndString.template operator()< Is >( strs ) ... );
    };

    return helper( std::make_index_sequence< sizeof...( Ns ) >{} );
}


template< unsigned int u >
inline constexpr auto toCTS() {

    if constexpr (u < 10) {
        return CompileTimeStr{ { '0' + u, '\0' } };
    }
    else {
        return toCTS< u / 10 >() + toCTS< u % 10 >();
    }
}


template< int n >
inline constexpr auto toCompileTimeStr() {

    if constexpr( n < 0 ) {
        return "-"_c + toCTS< -n >();
    }
    else {
        return toCTS< n >();
    }
}