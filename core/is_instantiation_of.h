#pragma once

/**
 * Is the given type an instantiation of the given template?
 * 
 * Note: the template cannot have non-type parameters.
*/
template< typename, template< typename... > typename >
inline constexpr bool IsInstantationOf = false;

template< template< typename... > typename Template, typename... TypeArgs >
inline constexpr bool IsInstantationOf< Template< TypeArgs... >, Template > = true;


/**
 * Like IsInstantationOf, but the template's first parameter is a non-type one.
*/
template< typename, template< auto, typename... > typename >
inline constexpr bool IsInstantationOfNTT = false;

template< template< auto, typename... > typename Template, auto nonTypeArg, typename... TypeArgs >
inline constexpr bool IsInstantationOfNTT< Template< nonTypeArg, TypeArgs... >, Template > = true;
