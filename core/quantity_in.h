#pragma once

#include "unit_of_measure.h"

#include <ostream>
#include <sstream>
#include <string>


template< UnitOfMeasure From, UnitOfMeasure To >
requires sameBaseUnitPowers< From, To >
inline constexpr auto conversionFactor = toDouble<

    DivideRationals< MultiplierVsBasePowers< From >, MultiplierVsBasePowers< To > >
>;


template< UnitOfMeasure U >
class QuantityIn {
public:

    explicit QuantityIn( double v = 0 ) : _value{ v } {}

    QuantityIn( const QuantityIn & ) = default;

    template< UnitOfMeasure OtherUnit >
    requires sameBaseUnitPowers< U, OtherUnit >
    explicit QuantityIn( const QuantityIn< OtherUnit > & other )
    : _value{ conversionFactor< OtherUnit, U > * other.value() }
    {}

    QuantityIn & operator = ( const QuantityIn & ) = default;

    QuantityIn & operator += ( QuantityIn other ) { _value += other._value; return *this; }

    QuantityIn & operator -= ( QuantityIn other ) { _value -= other._value; return *this; }

    QuantityIn operator - () const { return QuantityIn{ -_value }; }

    auto operator <=> ( const QuantityIn & ) const = default;

    template< UnitOfMeasure OtherUnit >
    requires sameBaseUnitPowers< U, OtherUnit >
    QuantityIn< OtherUnit > in() const {

        return QuantityIn< OtherUnit >{ conversionFactor< U, OtherUnit > * _value };
    }


    static auto unitStr() { return U::str(); }

    std::string str() const {

        std::ostringstream strm;

        const auto space = unitStr().length > 0 ? " " : "";

        strm << _value << space << U::str();

        return strm.str();
    }

    double value() const { return _value; }

private:

    double _value;
};


template< UnitOfMeasure U >
inline auto operator + ( QuantityIn< U > q1, QuantityIn< U > q2 ) {

    return QuantityIn< U >{ q1.value() + q2.value() };
}


template< UnitOfMeasure U >
inline auto operator - ( QuantityIn< U > q1, QuantityIn< U > q2 ) {

    return QuantityIn< U >{ q1.value() - q2.value() };
}


template< UnitOfMeasure U >
inline auto operator * ( QuantityIn< U > q, double c ) {

    return QuantityIn< U >{ q.value() * c };
}


template< UnitOfMeasure U >
inline auto operator * ( double c, QuantityIn< U > q ) {

    return QuantityIn< U >{ c * q.value() };
}


template< UnitOfMeasure U >
inline auto operator / ( QuantityIn< U > q, double c ) {

    return QuantityIn< U >{ q.value() / c };
}


template< UnitOfMeasure U >
inline auto operator / ( double c, QuantityIn< U > q ) {

    return QuantityIn< Inverse< U > >{ c / q.value() };
}


template< UnitOfMeasure U1, UnitOfMeasure U2 >
inline auto operator * ( QuantityIn< U1 > q1, QuantityIn< U2 > q2 ) {

    return QuantityIn< Product< U1, U2 > >{ multiplierInProduct< U1, U2 > * q1.value() * q2.value() };
}


template< UnitOfMeasure U1, UnitOfMeasure U2 >
inline auto operator / ( QuantityIn< U1 > q1, QuantityIn< U2 > q2 ) {

    return QuantityIn< Ratio< U1, U2 > >{ multiplierInRatio< U1, U2 > * q1.value() / q2.value() };
}


template< UnitOfMeasure U1, UnitOfMeasure U2 >
requires sameBaseUnitPowers< U1, U2 >
auto operator <=> ( const QuantityIn< U1 > & q1, const QuantityIn< U2 > & q2 ) {

    return q1.operator <=> ( QuantityIn< U1 >{ q2 } );
}


/**
 * Design note: without this, comparisons like 1_m == 1_km don't work: for some reason, the compiler
 * doesn't generate `< U1, U2 > operator ==` from `< U1, U2 > operator <=>`, regardless of whether the
 * latter is a member of QuantityIn or not.
 */
template< UnitOfMeasure U1, UnitOfMeasure U2 >
requires sameBaseUnitPowers< U1, U2 >
bool operator == ( const QuantityIn< U1 > & q1, const QuantityIn< U2 > & q2 ) {

    return ( q1 <=> q2 ) == 0;
}


template< UnitOfMeasure U >
std::ostream & operator << ( std::ostream & strm, QuantityIn< U > q ) {

    return strm << q.str();
}
