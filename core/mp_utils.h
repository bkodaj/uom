#pragma once

/**
 * Purpose of this file: provide some additional metaprogramming tools implemented on top of mp11.
 */

#include "int.h"

#include <boost/mp11.hpp>


using boost::mp11::mp_is_set;

template< typename List >
inline constexpr auto distinct = mp_is_set< List >::value;


using boost::mp11::mp_sort,
      boost::mp11::mp_same;

/**
 * Are the elements of the given list in sorted order by the given strict weak ordering?
 */
template< typename List, template< typename, typename > typename Ordering >
inline constexpr auto sorted = mp_same< List, mp_sort< List, Ordering > >::value;


using boost::mp11::mp_apply,
      boost::mp11::mp_transform,
      boost::mp11::mp_first,
      boost::mp11::mp_copy_if_q,
      boost::mp11::mp_second;

/**
 * @param ListOfPairs = [ ( U1, V1 ), ..., ( U_n, V_n ) ]
 *
 * @returns  [ F< U1 >, ..., F< U_n > ]
 */
template< template< typename ... > typename F, typename ListOfPairs >
using ApplyToFirst = mp_apply< F, mp_transform< mp_first, ListOfPairs > >;


/**
 * @param ListOfPairs = [ ( U1, V1 ), ..., ( U_n, V_n ) ]
 *
 * @param Q  A predicate in the form of a quoted metafunction.
 *
 * @returns  [ V_q1, ..., V_q_k ], where q1, ...q_k are the indices of those V's for which Q::template fn< V > is true.
 */
template< typename Q, typename ListOfPairs >
using FilterSecond = mp_copy_if_q< mp_transform< mp_second, ListOfPairs >, Q >;


using boost::mp11::mp_apply,
      boost::mp11::mp_plus;

template< typename ListOfInts >
using Sum = mp_apply< mp_plus, ListOfInts >;


using boost::mp11::mp_repeat,
      boost::mp11::mp_list;

template< typename T, NonNegativeInt N >
using RepeatN = mp_repeat< mp_list< T >, N >;


using boost::mp11::mp_same,
      boost::mp11::mp_size,
      boost::mp11::mp_contains;

template< typename List >
inline constexpr auto allSame = mp_apply< mp_same, List >::value;

template< typename List >
inline constexpr auto notEmpty = mp_size< List >::value > 0;

template< typename List, typename V >
inline constexpr auto contains = mp_contains< List, V >::value;


using boost::mp11::mp_eval_if_q,
      boost::mp11::mp_bool;

template< template< typename > typename F >
struct Apply {

    template< typename Arg > using fn = F< Arg >;
};

/**
 * Depending on the given condition, evaluate exactly one of the specified metafunctions.
 *
 * @returns  If C is true, F1< Arg >; otherwise, F2< Arg >.
 *
 * Useful when mp_eval_if_c< C, F1< Arg >, F2, Arg > doesn't compile because F1< Arg > is invalid
 * (e.g., due to constraint violations).
 */
template< bool C, typename Arg,
    template< typename > typename F1,
    template< typename > typename F2
>
using EvalEitherOr = mp_eval_if_q< mp_bool< C >,

    mp_eval_if_q< mp_bool< !C >, void, Apply< F1 >, Arg >,

    Apply< F2 >, Arg
>;
