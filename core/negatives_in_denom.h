#pragma once

#include "unit_power.h"
#include "compile_time_str.h"
#include "int.h"

#include <boost/mp11.hpp>


using boost::mp11::mp_apply;

template< UnitPower... UPs >
struct ProductStr {

    static inline constexpr auto str{ intersperse( " * "_c, UPs::str()... ) };
};

template< typename ListOfUnitPowers >
inline constexpr auto productStr = mp_apply< ProductStr, ListOfUnitPowers >::str;


using boost::mp11::mp_copy_if_q,
      boost::mp11::mp_list,
      boost::mp11::mp_size,
      boost::mp11::mp_transform;

/**
 * Purpose: take a sequence of unit powers and render them as a string like this:
 *
 *     product of unit powers with positive exponents
 *
 *     /
 *
 *     the _reciprocal_ of the product of unit powers with negative exponents
 *
 * E.g.,
 *   - m --> "m"
 *   - kg * m^2 --> "kg * m^2"
 *   - kg * m * s^-2 --> "kg * m / s^2"
 *   - kg * m^-1 * s^-1 --> "kg / (m * s)"
 */
template< UnitPower... UPs >
class NegativesInDenom {
public:

    static consteval auto str() { return num() + slashAndDenom(); }

private:

    using PositivePowers = mp_copy_if_q< mp_list< UPs... >, ExponentCondition< IsPositive > >;

    using NegativePowers = mp_copy_if_q< mp_list< UPs... >, ExponentCondition< IsNegative > >;

    static inline constexpr auto
        _numberOfPositivePowers = mp_size< PositivePowers >::value,
        _numberOfNegativePowers = mp_size< NegativePowers >::value;


    static consteval auto num() {

        if constexpr ( _numberOfPositivePowers > 0 ) {
            return productStr< PositivePowers >;
        }
        else if constexpr ( _numberOfNegativePowers > 0 ) {
            return "1"_c;
        }
        else {
            return ""_c;
        }
    }


    static consteval auto slashAndDenom() {

        if constexpr ( _numberOfNegativePowers > 0 )  {
            return " / "_c + paren( productStr< mp_transform< InversePower, NegativePowers > > );
        }
        else {
            return ""_c;
        }
    }


    template< std::size_t S >
    static consteval auto paren( const CompileTimeStr< S > & negativePowersStr ) {

        if constexpr ( _numberOfNegativePowers > 1 ) {
            return "("_c + negativePowersStr + ")"_c;
        }
        else {
            return negativePowersStr;
        }
    }
};
