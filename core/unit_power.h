#pragma once

#include "base_unit.h"
#include "compile_time_str.h"
#include "int.h"
#include "is_instantiation_of.h"


/**
 * A base unit or a multiple thereof raised to an integer power: e.g., m^2, s^-1, km^3, etc.
 */
template< BaseOrMultiple U, Int Exp >
struct RaiseToPower {

    using Unit = U;

    using Exponent = Exp;

    static consteval auto str() {

        constexpr auto exp = Exp::value;

        if constexpr ( exp == 0 ) {
            return ""_c;
        }
        else if constexpr ( exp == 1 ) {
            return U::str();
        }
        else {
            return U::str() + "^"_c + toCompileTimeStr< exp >();
        }
    }
};

template< typename T >
concept UnitPower = IsInstantationOf< T, RaiseToPower >;

template< UnitPower UP >
using Unit = typename UP::Unit;

template< UnitPower UP >
using Exponent = typename UP::Exponent;


template< UnitPower UP >
using InversePower = RaiseToPower< Unit< UP >, Minus< Exponent< UP > > >;


template< UnitPower UP >
using MultiplierPower = Power< Multiplier< Unit< UP > >, Exponent< UP > >;


template< UnitPower UP >
using BaseUnitPower = RaiseToPower< Base< Unit< UP > >, Exponent< UP > >;


template< UnitPower Left, UnitPower Right >
using LessByBaseUnitSymbol = LessBySymbol< Base< Unit< Left > >, Base< Unit< Right > > >;


template< template< Int > typename IntCondition >
struct ExponentCondition {

    template< UnitPower UP > using fn = IntCondition< Exponent< UP > >;
};
