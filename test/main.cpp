#include "units.h"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>


template< typename T, typename U >
concept CanBeAdded = requires ( T t, U u ) { t + u; };

/**
 * Needed because std::is_equality_comparable_with< Meters, Kilometers > is false.
 */
template< typename T, typename U >
concept CanBeCompared = requires( T t, U u ) {

    t == u; t != u; t <= u; t >= u; t < u; t > u;
    u == t; u != t; u <= t; u >= t; u < t; u > t;
};



TEST_CASE( "Arithmetic operations" ) {

    SECTION( "Addition" ) {

        REQUIRE( CanBeAdded< Meters, Meters > );

        REQUIRE( !CanBeAdded< Meters, Seconds > );

        REQUIRE( !CanBeAdded< Meters, Kilometers > );

        REQUIRE( ( 1_m + 1_m ).str() == "2 m" );
    }


    SECTION( "Multiplication and division" ) {

        REQUIRE( ( 1_m / 1_s ).str() == "1 m / s" );

        REQUIRE( ( 1 / 1_s ).str() == "1 1 / s" );

        REQUIRE( ( 1_m / 1_m ).str() == "1" );

        REQUIRE( ( 1_kg / ( 1_m * 1_s ) ).str() == "1 kg / (m * s)" );

        REQUIRE( 1 / ( 1 / 1_s ) == 1_s );

        REQUIRE( 1_m / 1_s * 1_s == 1_m );
    }
}


TEST_CASE( "Relational operators" ) {

    REQUIRE( CanBeCompared< Meters, Meters > );

    REQUIRE( !CanBeCompared< Meters, Seconds > );

    REQUIRE( CanBeCompared< Meters, Kilometers > );

    REQUIRE( CanBeCompared< Seconds, Hours > );


    SECTION( "Base units" ) {

        REQUIRE( 1_m == 1_m );

        REQUIRE( !( 1_m == 2_m ) );

        REQUIRE( 1_m != 2_m );

        REQUIRE( 1_m <= 1_m );

        REQUIRE( 1_m >= 1_m );

        REQUIRE( 1_m <= 2_m );

        REQUIRE( 2_m >= 1_m );

        REQUIRE( 1_m < 2_m );

        REQUIRE( 2_m > 1_m );
    }


    SECTION( "Multiples of base units" ) {

        REQUIRE( 1_km == 1_km );

        REQUIRE( !( 1_km == 2_km ) );

        REQUIRE( 1_km != 2_km );

        REQUIRE( 1_km <= 1_km );

        REQUIRE( 1_km >= 1_km );

        REQUIRE( 1_km <= 2_km );

        REQUIRE( 2_km >= 1_km );

        REQUIRE( 1_km < 2_km );

        REQUIRE( 2_km > 1_km );

    }


    SECTION( "Base units vs their multiples" ) {

        REQUIRE( !( 1_m == 1_km ) );

        REQUIRE( 1_m != 1_km );

        REQUIRE( 1_m < 1_km );

        REQUIRE( 1_m <= 1_km );

        REQUIRE( 1_km > 1_m );

        REQUIRE( 1_km >= 1_m );
    }
}


TEST_CASE( "Derived units" ) {

    REQUIRE( Ratio< Meter, Second >::str() == "m / s"_c );

    REQUIRE( Ratio< Product< Kilogram, Meter >, Product< Second, Second > >::str() == "kg * m / s^2"_c );

    REQUIRE( Inverse< Second >::str() == "1 / s"_c );


    REQUIRE( ( 100_USD / 1_FC ).unitStr() == "USD / FC"_c );

    REQUIRE( ( 50_USD / 1_FH ).unitStr() == "USD / FH"_c );

    REQUIRE( !CanBeAdded< Ratio< UsDollar, FlightCycle >, Ratio< UsDollar, FlightHour > > );
}


TEST_CASE( "Multiples of base units" ) {

    REQUIRE( ( 1_km + 1_km ).str() == "2 km" );

    REQUIRE( !CanBeAdded< Meters, Kilometers > );

    REQUIRE( ( 1_km / 1_h ).str() == "1 km / h" );

    REQUIRE( 1_km / 1_h * 3600_s == 1_km );

    REQUIRE( ( 1_km * 1_m ).str() == "1000 m^2" );
}


TEST_CASE( "Named derived units" ) {

    REQUIRE( Hertzes{ 440 / 1_s }.str() == "440 Hz" );

    REQUIRE( 440_Hz * 1_s == 440_dl );


    REQUIRE( Newtons{ 1_kg * 1_m / ( 1_s * 1_s ) }.str() == "1 N" );

    REQUIRE( 1_N * 1_s * 1_s / 1_kg == 1_m );


    REQUIRE( Joules{ 1_kg * 1_m * 1_m / ( 1_s * 1_s ) }.str() == "1 J" );

    REQUIRE( Joules{ 1_N * 1_m }.str() == "1 J" );

    REQUIRE( 1_J / 1_kg / 1_m / 1_m == 1 / 1_s / 1_s );
}


TEST_CASE( "Conversion" ) {

    using KmPerHour = Ratio< Kilometer, Hour >;

    REQUIRE( ( 1_m / 1_s ).in< KmPerHour >().value() == 3.6 );

    REQUIRE( ( 1_N * 1_m ).in< Joule >() == 1_J );
}


