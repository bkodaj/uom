#pragma once

#include <uom.h>


using Meter = DefineBaseUnit< "m" >;

using Meters = QuantityIn< Meter >;

inline auto operator ""_m( long double v ) { return Meters{ static_cast< double >( v ) }; }

inline auto operator ""_m( unsigned long long v ) { return Meters{ static_cast< double >( v ) }; }


using Kilometer = MultiplyBaseUnit< "km", std::ratio< 1000, 1 >, Meter >;

using Kilometers = QuantityIn< Kilometer >;

inline auto operator ""_km( long double v ) { return Kilometers{ static_cast< double >( v ) }; }

inline auto operator ""_km( unsigned long long v ) { return Kilometers{ static_cast< double >( v ) }; }


using Second = DefineBaseUnit< "s" >;

using Seconds = QuantityIn< Second >;

inline auto operator ""_s( long double v ) { return Seconds{ static_cast< double >( v ) }; }

inline auto operator ""_s( unsigned long long v ) { return Seconds{ static_cast< double >( v ) }; }


using Hour = MultiplyBaseUnit< "h", std::ratio< 3600, 1 >, Second >;

using Hours = QuantityIn< Hour >;

inline auto operator ""_h( long double v ) { return Hours{ static_cast< double >( v ) }; }

inline auto operator ""_h( unsigned long long v ) { return Hours{ static_cast< double >( v ) }; }


using Kilogram = DefineBaseUnit< "kg" >;

using Kilograms = QuantityIn< Kilogram >;

inline auto operator ""_kg( long double v ) { return Kilograms{ static_cast< double >( v ) }; }

inline auto operator ""_kg( unsigned long long v ) { return Kilograms{ static_cast< double >( v ) }; }


using Hertz = NameDerivedUnit< "Hz", Inverse< Second > >;

using Hertzes = QuantityIn< Hertz >;

inline auto operator ""_Hz( long double v ) { return Hertzes{ static_cast< double >( v ) }; }

inline auto operator ""_Hz( unsigned long long v ) { return Hertzes{ static_cast< double >( v ) }; }


using Newton = NameDerivedUnit< "N", Ratio< Product< Kilogram, Meter >, Product< Second, Second > > >;

using Newtons = QuantityIn< Newton >;

inline auto operator ""_N( long double v ) { return Newtons{ static_cast< double >( v ) }; }

inline auto operator ""_N( unsigned long long v ) { return Newtons{ static_cast< double >( v ) }; }


using Joule = NameDerivedUnit< "J", Ratio< Product< Kilogram, Meter, Meter >, Product< Second, Second > > >;

using Joules = QuantityIn< Joule >;

inline auto operator ""_J( long double v ) { return Joules{ static_cast< double >( v ) }; }

inline auto operator ""_J( unsigned long long v ) { return Joules{ static_cast< double >( v ) }; }


using UsDollar = DefineBaseUnit< "USD" >;

using UsDollars = QuantityIn< UsDollar >;

inline auto operator ""_USD( long double v ) { return UsDollars{ static_cast< double >( v ) }; }

inline auto operator ""_USD( unsigned long long v ) { return UsDollars{ static_cast< double >( v ) }; }


using FlightCycle = DefineBaseUnit< "FC" >;

using FlightCycles = QuantityIn< FlightCycle >;

inline auto operator ""_FC( long double v ) { return FlightCycles{ static_cast< double >( v ) }; }

inline auto operator ""_FC( unsigned long long v ) { return FlightCycles{ static_cast< double >( v ) }; }


using FlightHour = DefineBaseUnit< "FH" >;

using FlightHours = QuantityIn< FlightHour >;

inline auto operator ""_FH( long double v ) { return FlightHours{ static_cast< double >( v ) }; }

inline auto operator ""_FH( unsigned long long v ) { return FlightHours{ static_cast< double >( v ) }; }


using Dimensionless = QuantityIn< MultiplyUnitPowers<> >;

inline auto operator ""_dl( long double v ) { return Dimensionless{ static_cast< double >( v ) }; }

inline auto operator ""_dl( unsigned long long v ) { return Dimensionless{ static_cast< double >( v ) }; }


