What's this?
============
Just a proof-of-concept exercise in using modern C++ to represent units of measure at compile time.


Why did I write it?
===================
The use case that led me to writing this library was the following: I was working in TypeScript on an application that
dealt with aircraft lease contracts. Such leases often stipulate that at the end of each month, the airline using the
aircraft has to pay certain amounts defined as _utilization * rate_, where:
  - Utilization expresses how much the plane flew in that month. It is measured in two units:
    - _Flight cycles (FC)_ : One FC is one "take off, fly, land" sequence.
    - _Flight hours (FH)_ : One FH is one hour elapsed during a flight cycle.
  - Rate is defined either in $ / FC or in $ / FH.
  
The application sometimes had to compute the sum of rates, e.g., to calculate the overall rate for an engine from
the rates for its parts. Because the TypeScript code represented all rates as plain [`number`s](https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#the-primitives-string-number-and-boolean),
it was possible for a $ / FC rate to be added to a $ / FH rate without the compiler telling you that this didn't
make sense. Since there were plans to rewrite the application in C++, I started thinking about how I could model
$ / FH and $ / FC rates in C++ in such a way that it would be a compile-time error to add one to the other.   

Of course, I was aware of the fact that there are dozens of existing unit-of-measure libraries: e.g., see a compilation
[here](https://github.com/martinmoene/PhysUnits-CT-Cpp11#other-libraries), especially Boost.Units and
[mp_units](https://github.com/mpusz/units). However, all of them were too cumbersome for my purposes, because I wanted
to make the definition of *custom units* as simple as possible. E.g., in the use case above, I wanted to be able to
write this:

```C++
using FlightCycle = DefineUnit< "FC" >;
inline auto operator ""_FC( ... ){ ... }

using FlightHour = DefineUnit< "FH" >;
inline auto operator ""_FH( ... ){ ... }

using UsDollar = DefineUnit< "USD" >;
inline auto operator ""_USD( ... ){ ... }

const auto rate1 = 100_USD / 1_FC;
const auto rate2 =  50_USD / 1_FH;
std::cout
    << rate1 << "\n"  // Must print "100 USD / FC"
    << rate2 << "\n"; // Must print "50 USD / FH"

const auto sum = rate1 + rate2; // Must not compile
```

That is, I wanted unit definition to be as simple as supplying the unit's __symbol__ as a C string literal - unfortunately,
that is not possible in any of the existing libraries:. Therefore, although in general I really dislike the Not Invented Here syndrome, I decided to write my own proof-of-concept library.


Goals
=====
So, I set out to write a library with the following properties:

- Distinct units of measure are represented as distinct types.

- Those types don't incur any runtime overhead: e.g., adding two objects of type `meter` should be as efficient as
  adding two plain `double`s.

- Arithmetic operations involving objects of unit-of-measure types behave intuitively.
  E.g.,
  - `std::cout << oneMeter + oneMeter` prints `2 m`.
  - `oneMeter + oneSecond` does not compile.
  - `std::cout << oneMeter / oneSecond` prints `1 m / s`.
  - `std::cout << 1 / oneSecond` prints `1 1 / s`.
  - `std::cout << oneMeter / oneMeter` prints `1`.
  - `oneMeter / oneSecond * oneSecond` is equal to `oneMeter`.

- You can define any number of *base units* and create *derived units* from them via multiplying and dividing
  either the base unit types themselves or objects of those types.
  - E.g., if you define types for the [SI base units](https://en.wikipedia.org/wiki/SI_base_unit) meter, second, etc.,
    then you can define types for [derived units](https://en.wikipedia.org/wiki/SI_derived_unit) like this:
    - m / s: `Ratio< meter, second >`
    - kg * m / s^2: `Ratio< Product< kilogram, meter >, Product< second, second > >`
    - 1 / s: `Inverse< second >`


- You can define *multiples* of base units that are treated atomically (i.e., they're not reverted to the
  underlying base unit) when possible.
  - E.g., if kilometer is defined as 1000 * m and hour as 3600 * s, then:
    - `std::cout << oneKm + oneKm` prints `2 km`.
    - `std::cout << oneKm / oneHour` prints `1 km / h`.
  - Note, however, that mixing a base unit and its multiples will sometimes require that the multiples be *reverted*
    to the base unit.
    - E.g., `oneKm * oneMeter` will be `1000 m^2` - consider that the library cannot know whether the user prefers
      to express the result in km^2 or m^2, so reverting to m^2 is the reasonable choice.

- You can *name* derived units. E.g., in SI:
  - Hz = 1 / s
  - N = kg * m / s^2
  - J = kg * m^2 / s^2

- Defining a base unit type is simple.
  - It's enough to specify a unique *symbol* like `m`, `s`, `FC`, etc., for creating a type that represents the
    unit of measure corresponding to the symbol.


Implementation principles
=========================
- Modeling operations like multiplication and division between unit-of-measure types will obviously require template
  metaprogramming. Don't reinvent the wheel: use an existing, high-quality, well-tested metaprogramming library.
  - [Boost.Mp11](https://www.boost.org/doc/libs/develop/libs/mp11/doc/html/mp11.html) is relatively simple but powerful,
    and it turned out to be perfect for my purposes.

- Use modern C++ features, up to and including C++20, when 1. they make sense, 2. they're available in the current
  GCC release. (Apparently, GCC is further along than Clang in the implementation of most C++20 features.)

  E.g.,
  - [`operator <=>`](core/quantity_in.h) lets you have the compiler generate all relational operators for your type.
  - [`concept`s](core/base_unit.h) are to class template parameters what types are to function parameters.
  - [Non-type template parameters of class type](core/compile_time_str.h) make it possible to do this:
    ```C++
    using meter = DefineBaseUnit< "m" >;
    ```

- In general:
  - Keep the code simple and reasonably easy to understand.
  - Use comments judiciuosly: prefer identifiers and language constructs to express meaning.
  - However:
    - Do provide brief explanations for basic building blocks like [base units](core/base_unit.h),
      [derived units](core/derived_unit.h), etc.
    - Do provide examples for [more complex parts](core/normalize.h) of the code.


Building and examples
=====================
You need:
- gcc 13.1 or later
- CMake

See `test/main.cpp` and `test/units.h` for how to define base units and their multiples, name derived units, etc.



UPDATE (September 2023)
=======================
Three years later, I revisited this project to find out whether the most recent GCC (13.1 at the time of writing) allowed me to improve the code further. The answer was "yes, but not spectacularly".


Label trick no longer needed
----------------------------
- I need to be able to express the following as a C++ concept: "type `T` is an instantiation of template `U`".
- This is possible via partial template specialization, but GCC 10 had a [bug](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=95986) that prevented me from doing that.
- Therefore I had to resort to the following trick:
  - [Attach](https://gitlab.com/bkodaj/uom/-/blob/2ba5f8dfb486152e3889035271ccf403335de14e/core/base_unit.h#L15) a [label](https://gitlab.com/bkodaj/uom/-/blob/2ba5f8dfb486152e3889035271ccf403335de14e/core/label.h), i.e., a compile-time string constant, to every template `U` for which I need the concept in question to work.
  - With that, the concept reduces to [checking](https://gitlab.com/bkodaj/uom/-/blob/2ba5f8dfb486152e3889035271ccf403335de14e/core/base_unit.h#L23) whether `T` has the label attached to `U`.
- The trick is now no longer needed because the bug was fixed in GCC 12, and now I'm able to use [partial template specialization](core/is_instantiation_of.h#L9) as I wanted originally.
  - Note, however, that it's still not possible to do this in a completely generic way. That is, I cannot define an `IsInstantiationOf` that can handle any template `U` with any number of type and non-type parameters in any order.
  - So, I had to resort to defining two versions: one for templates with only type parameters and [another one](core/is_instantiation_of.h#L19) for templates with one non-type parameter.


C++20 modules are still a work in progress
------------------------------------------
I made an attempt to use modules instead of header files. However, I ran into problems when my modules needed to include standard headers like `iostream` or `algorithm`: GCC started emitting obscure error messages like "recursive lazy load". After some debugging, I gave up - it wasn't worth the effort. Apparently, module support in GCC still has a long way to go. 
  - Note also that, at the time of writing, support for modules is rudimentary in CMake as well, and you need to build a patched GCC for it to work as explained [here](https://www.youtube.com/watch?v=c563KgO-uf4&t=3438s).
